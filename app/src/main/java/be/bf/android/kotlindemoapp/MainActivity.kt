package be.bf.android.kotlindemoapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.bf.android.kotlindemoapp.adapters.UserAdapter
import be.bf.android.kotlindemoapp.dal.entities.User
import be.bf.android.kotlindemoapp.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels {
        MainViewModelFactory(this@MainActivity)
    }

    private val users = mutableListOf<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViewModel()

        binding.users.layoutManager = LinearLayoutManager(this)
        binding.users.adapter = UserAdapter(this@MainActivity, users)

        binding.add.setOnClickListener { viewModel.addUser(User("Gregory", "Le commercial")) }

//        binding.redirect.setOnClickListener(this::toCounterActivity)
    }

    private fun initViewModel() {
        viewModel.count.observe(this, this::countListener)
        viewModel.users.observe(this, this::usersListener)
    }

    private fun usersListener(it: List<User>) {
        Log.d(TAG, "usersListener: $it")
//        users.clear()
        users.addAll(it)
        binding.users.adapter?.notifyDataSetChanged()
    }
    private fun countListener(it: Int) {
        Log.d(TAG, "countListener: $it")
//        binding.helloWorld.text = "${binding.helloWorld.text} + ${it}"
    }
//
//    private fun toCounterActivity(v: View) {
//        val intent = Intent(this, CountActivity::class.java)
//        startActivity(intent)
//    }
}