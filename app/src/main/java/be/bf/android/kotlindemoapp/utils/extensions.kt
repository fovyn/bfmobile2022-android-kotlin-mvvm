package be.bf.android.kotlindemoapp.utils

import android.database.Cursor

infix fun Cursor.getString(column: String): String = getString(getColumnIndex(column))
infix fun Cursor.getInt(column: String): Int = getInt(getColumnIndex(column))
infix fun Cursor.getLong(column: String): Long = getLong(getColumnIndex(column))
infix fun Cursor.getDouble(column: String): Double = getDouble(getColumnIndex(column))
infix fun Cursor.getFloat(column: String): Float = getFloat(getColumnIndex(column))
infix fun Cursor.getBlop(column: String): ByteArray = getBlob(getColumnIndex(column))
infix fun Cursor.getShort(column: String): Short = getShort(getColumnIndex(column))
