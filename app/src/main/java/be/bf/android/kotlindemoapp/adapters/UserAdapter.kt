package be.bf.android.kotlindemoapp.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.bf.android.kotlindemoapp.R
import be.bf.android.kotlindemoapp.dal.entities.User

class UserAdapter(
    val context: Context,
    private val list: List<User>
): RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_user,parent, false)
        view.setOnClickListener {
            Log.d("UserAdapter", "onClick: ${it.findViewById<TextView>(R.id.user_username).text}")
        }

        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user: User = list[position]
        holder.username.text = user.username
        holder.password.text = user.password
    }

    override fun getItemCount(): Int = list.size


    class UserViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val username: TextView = view.findViewById(R.id.user_username)
        val password: TextView = view.findViewById(R.id.user_password)
    }
}