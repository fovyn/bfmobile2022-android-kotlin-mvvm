package be.bf.android.kotlindemoapp

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import be.bf.android.kotlindemoapp.dal.DbHelper
import be.bf.android.kotlindemoapp.dal.dao.UserDao

class MainViewModelFactory(private val context: Context): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val helper = DbHelper(context)
        return MainViewModel(UserDao(helper)) as T
    }
}