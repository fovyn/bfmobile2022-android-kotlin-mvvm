package be.bf.android.kotlindemoapp

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import be.bf.android.kotlindemoapp.dal.dao.UserDao
import be.bf.android.kotlindemoapp.dal.entities.User
import kotlinx.coroutines.launch

class MainViewModel(val dao: UserDao): ViewModel() {
    private val _users = mutableListOf<User>()
    val users: MutableLiveData<List<User>> = MutableLiveData(_users)

    private var _count: MutableLiveData<Int> = MutableLiveData(_users.size)
    val count: LiveData<Int>
        get() = _count

    init {
        viewModelScope.launch {
            dao.findAll().collect {
                it.onEach { user -> println(user) }
                users.value = it
                _count.value = it.size
            }
        }
    }

    fun addUser(user: User) {
        dao.insert(user)
        _users.add(user)
        _count.value = _users.size
        users.value = _users
    }
}